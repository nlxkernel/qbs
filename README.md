#QBS
---
QBS (or quick boot strap) is a simple barebones bootloader designed for loading NLX

###Why not use GRUB?
You can use grub with NLX, but I wanted to understand fully how a kernel is loaded just as much as how they work, so this is the result
